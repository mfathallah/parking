class ParkingPage {
    
    //selectors
    get ParkingNameSelector () { return $('#ParkingLot') }

    //methodes

    selectParking (parkingName) {
        //return browser.url(`https://the-internet.herokuapp.com/${path}`)
        this.ParkingNameSelector.selectByVisibleText(parkingName)

    }
}

module.exports = new ParkingPage();
