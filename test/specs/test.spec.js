const ParkingPage = require('../pageobjects/Parking.page');

var assert = require('assert');


describe('parking feature', () => {
    it('should display the correct homepage title', () => {
        // browser.url('http://www.shino.de/parkcalc/index.php');
        const title = browser.getTitle();
        assert.equal(title, 'Parking Cost Calculator');
    });

    it(' Short-Term Parking cost calculation', () => {
        //selectors
        // const selectBox = $('#ParkingLot');
        const startingDate = $('#StartingDate')
        const LeavingDate = $('#LeavingDate');
        const LeavingTime = $('#LeavingTime')
        const buttonCalculate = $("body:nth-child(2) form:nth-child(2) > input:nth-child(3)")
        const cost = $('.SubHead > b');
        costContent = cost.getText();

        //actions
        // selectBox.selectByVisibleText('Short-Term Parking')
        // with POM
        ParkingPage.selectParking('Short-Term Parking')
        browser.pause(5000)

        //NO POM
        startingDate.setValue('7/29/2020')
        LeavingDate.setValue('7/29/2020')
        LeavingTime.setValue('14:00')
        buttonCalculate.click()
        browser.pause(5000)
        cost.waitForDisplayed();
        assert.equal(costContent, '$ 24.00');


        // selectBox.click();
        // const value = selectBox.getValue();
        // console.log(value); 
        
    });



});


89